# -*- coding: utf-8 -*-
"""
Created on Tue Mar  8 13:33:51 2022

@author: lucie
"""
# Fichier comportant les fonctions pour trouver le CAP 
# avec des SET à 4 cartes
# pour 4 valeurs
# pour 2, 3 ou 4 attributs

import numpy as np
import random as r
from itertools import product

#creation et initialisation d'un paquet de carte pour un nombre de :
#valeur val
#attribut att
def creation_paquet(val,att):
    jeu = []
    temp1 = []
    temp2 = []

    for i in range(val):
        temp1.append(i)
    for j in range(att):
        temp2.append(temp1)


    for elem in product(*temp2):
        jeu.append(elem)
        
    return jeu
    print(jeu)


#trouver les éléments manquants
def trouver_elts_manquants(a, b):
    
    l = list(range(4))
    res = []
    
    for i in range(len(l)):
        
        if (l[i]!=a and l[i]!=b) :
            res.append(l[i])
            
    return (res[0],res[1])


#touver la 4eme carte pour faire un SET (4 valeurs)
def trouver_set4(carte1,carte2,carte3):
    
    carte_set=[]
    att = len(carte2)
    set = True 
    val = 4
    i = 0
    
    while (set==True and i<att):
        
        if (carte1[i]==carte2[i] and carte1[i]==carte3[i] and carte2[i]==carte3[i]):
            carte_set.append(carte1[i])
            
        elif (carte1[i]!=carte2[i] and carte1[i]!=carte3[i] and carte2[i]!=carte3[i]):
            nb=(6-(carte1[i]+carte2[i]+carte3[i]))%val
            carte_set.append(nb)
            
        else :
            set = False
            carte_set = []
        i+=1
    
    return carte_set
  

#trouver la position d'une carte dans le paquet
def trouver_position(carte,val):
    
    position=0
    att = len(carte)
    
    for i in range(att):
        position += carte[i]*val**(att-1-i)
        
    return position



#Création de la liste des positions des cartes dans le jeu
# Liste des positions des cartes restantes à tirer
def creer_list_positions(val, att):
    
    n = val**(att)
    list_pos = []
    
    for i in range(n):
        list_pos.append(i)
        
    return(list_pos)


#dichotomie pour retrouver la position d'une carte.
def dicho(list_pos,position):
    ak = 0
    bk = len(list_pos)
    boolean = False
    
    while ak <= bk and boolean == False:
        
        milieu = (ak+bk)//2

        if list_pos[milieu] == position:
            boolean = True
        
        if position>list_pos[milieu]:
            ak = milieu + 1
        else :
            bk = milieu
            
    return milieu


#enlever une carte du jeu quand on a trouvé le set   
def enlever_carte(carte, jeu, list_pos, nb_cartes_supp,val):
    
    position=trouver_position(carte,val)
    
    if jeu[position]!=0 :
        jeu[position]=0
        supp = dicho(list_pos, position)
        del list_pos[supp]
        nb_cartes_supp+=1
    
    return (jeu,nb_cartes_supp)
 

#poser une carte sur un plateau
def poser_carte(jeu, list_pos:[int], plateau, val):

    nb_cartes_supp=0
    
    n=len(list_pos)
    i=r.randint(0,n-1)
    position=list_pos[i]
    del list_pos[i]
    carte1=jeu[position]
    
    
    for i in range(len(plateau)-1):

        carte2=plateau[i]
        
        for j in range (i+1,len(plateau)):
            
            carte3=plateau[j]
            carte_set = trouver_set4(carte1,carte2,carte3)
            
            if carte_set != []:
                jeu,nb_cartes_supp=enlever_carte(carte_set, jeu, list_pos, nb_cartes_supp,val)
                
    plateau.append(carte1)    
    jeu[position]=0   
    
    return (jeu, list_pos, plateau, nb_cartes_supp)


#trouver le cap pour une valeur donner 
def trouver_cap(val,att):
    
    jeu = creation_paquet(val,att)
    list_pos = creer_list_positions(val,att)
    plateau = []
    list_nb_cartes_supp = []

    
    while len(list_pos) != 0:
        jeu,list_pos,plateau,nb_cartes_supp = poser_carte(jeu, list_pos, plateau,val)
        list_nb_cartes_supp.append(nb_cartes_supp)

    cap = len(plateau)
    return cap


#trouver le cap de taille maximale pour une valeur donnée
def trouver_cap_max(val,att):
    
    cap_max=0
    
    for k in range (1000):
        
        cap = trouver_cap(val, att)
        
        if cap>cap_max :
            cap_max = cap
            
    return (cap_max)


#Calculer les pourcentages des CAP pour 2 attributs et 4 valeurs avec des SET de 4
def calcul_pourcentage2(nbiteration):
    
    cap8=0
    cap9=0
    val=4
    att=2
     
    for i in range(nbiteration):
        cap = trouver_cap(val,att)

        if cap==8 :
            cap8 +=1
        elif cap==9:
            cap9+=1
    
    print ('cap de 8 :', cap8*100/nbiteration, 'cap de 9 :', cap9*100/nbiteration, '%')
      

#Calculer les pourcentages des CAP pour 3 attributs et 4 valeurs avec des SET de 4
def calcul_pourcentage3(nbiteration): 
    
    val=4
    att=3
    cap19=0
    cap20=0
    cap21=0
    cap22=0
    cap23=0
    cap24=0
    cap25=0
    cap26=0
    cap27=0
    cap28=0
    autre=0

     
    for i in range(nbiteration):
        cap = trouver_cap(val,att)
        
        if cap==19 :
            cap19 +=1
            
        elif cap==20 :
            cap20 +=1
                
        elif cap==21 :
            cap21 +=1
            
        elif cap==22 :
            cap22 +=1
            
        elif cap==23 :
            cap23 +=1
            
        elif cap==24 :
            cap24 +=1
            
        elif cap==25 :
            cap25 +=1
            
        elif cap==26 :
            cap26 +=1
            
        elif cap==27 :
            cap27 +=1
            
        elif cap==28 :
            cap28 +=1
            
        else:
            autre=cap
        
    print ('cap de 16 :', cap19*100/nbiteration, '%\ncap de 20 :', cap20*100/nbiteration, '%\ncap de 21 :', cap21*100/nbiteration, '%\ncap de 22 :', cap22*100/nbiteration, '%\ncap de 23 :', cap23*100/nbiteration, '%\ncap de 24 :', cap24*100/nbiteration, '%\ncap de 25 :', cap25*100/nbiteration, '%\ncap de 26 :', cap26*100/nbiteration, '%\ncap de 27 :', cap27*100/nbiteration, '%\ncap de 28 :', cap28*100/nbiteration, '%')   


#Calculer les pourcentages des CAP pour 4 attributs et 4 valeurs avec des SET de 4
def calcul_pourcentage4(nbiteration): 
    
    val=4
    att=4
    cap49=0
    cap50=0
    cap51=0
    cap52=0
    cap53=0
    cap54=0
    cap55=0
    cap56=0
    cap57=0
    cap58=0
    cap59=0
    cap60=0
    cap61=0
    cap62=0
    cap63=0
    cap64=0
    cap65=0
    cap66=0
    cap67=0
    cap68=0
    cap69=0
    cap70=0
    cap71=0
    cap72=0
    cap73=0
    cap74=0
    cap75=0
    cap76=0
    cap77=0
    cap78=0
    autre=0

     
    for i in range(nbiteration):
        cap = trouver_cap(val,att)
        
        if cap==49 :
            cap49 +=1
        
        elif cap==50 :
            cap50 +=1
            
        elif cap==51 :
            cap51 +=1
            
        elif cap==52 :
            cap52 +=1
        
        elif cap==53 :
            cap53 +=1
            
        elif cap==54 :
            cap54 +=1
        
        elif cap==55 :
            cap55 +=1
            
        elif cap==56 :
            cap56 +=1
        
        elif cap==57 :
            cap57 +=1
            
        elif cap==58 :
            cap58 +=1
        
        elif cap==59 :
            cap59 +=1
        
        elif cap==60 :
            cap60 +=1
            
        elif cap==61 :
            cap61 +=1
        
        elif cap==62 :
            cap62 +=1
        
        elif cap==63 :
            cap63 +=1
        
        elif cap==64 :
            cap64 +=1
        
        elif cap==65 :
            cap65 +=1
            
        elif cap==66 :
            cap66 +=1
                
        elif cap==67 :
            cap67 +=1
            
        elif cap==68 :
            cap68 +=1
            
        elif cap==69 :
            cap69 +=1
            
        elif cap==70 :
            cap70 +=1
            
        elif cap==71 :
            cap71 +=1
            
        elif cap==72 :
            cap72 +=1
            
        elif cap==73 :
            cap73 +=1
            
        elif cap==74 :
            cap74 +=1
            
        elif cap==75 :
            cap75 +=1
        
        elif cap==76 :
            cap76 +=1
        
        elif cap==77 :
            cap77 +=1
            
        elif cap==78 :
            cap78 +=1
            
        else:
            autre=cap
        
    print ('cap de 50 :', cap50*100/nbiteration, '%\ncap de 51 :', cap51*100/nbiteration, '%\ncap de 52 :', cap52*100/nbiteration, '%\ncap de 53 :', cap53*100/nbiteration, '%\ncap de 54 :', cap54*100/nbiteration, '%\ncap de 55 :', cap55*100/nbiteration, '%\ncap de 56 :', cap56*100/nbiteration, '%\ncap de 57 :', cap57*100/nbiteration, '%\ncap de 58 :', cap58*100/nbiteration, '%\ncap de 59 :', cap59*100/nbiteration, '%\ncap de 60 :', cap60*100/nbiteration, '%\ncap de 61 :', cap61*100/nbiteration, '%\ncap de 62 :', cap62*100/nbiteration, '%\ncap de 63 :', cap63*100/nbiteration, '%\ncap de 64 :', cap64*100/nbiteration, '%\ncap de 65 :', cap65*100/nbiteration, '%\ncap de 66 :', cap66*100/nbiteration, '%\ncap de 67 :', cap67*100/nbiteration, '%\ncap de 68 :', cap68*100/nbiteration, '%\ncap de 69 :', cap69*100/nbiteration, '%\ncap de 70 :', cap70*100/nbiteration, '%\ncap de 71 :', cap71*100/nbiteration, '%\ncap de 72 :', cap72*100/nbiteration, '%\ncap de 73 :', cap73*100/nbiteration, '%\ncap de 74 :', cap74*100/nbiteration, '%\ncap de 75 :', cap75*100/nbiteration, '%\ncap de 76 :', cap76*100/nbiteration, '%\ncap de 77 :', cap77*100/nbiteration, '%\ncap de 78 :', cap78*100/nbiteration,'%', autre)   


#fonction principale du programme pour faire varier le nombre d'attributs avec 4 valeurs
def prog_principal():
    
    print('choisissez un nombre d attributs entre 2, 3 et 4')
    temp = input()
    attribut = int(temp)
    
    if attribut==2 :
        print('le cap de taille maximale pour 4 valeurs et 2 attributs est', trouver_cap_max(4,attribut))
        calcul_pourcentage2(1000)
    
    elif attribut==3 :
        print('le cap de taille maximale pour 4 valeurs et 3 attributs est', trouver_cap_max(4,attribut))
        calcul_pourcentage3(1000)
    
    else :
        print('le cap de taille maximale pour 4 valeurs et 4 attributs est', trouver_cap_max(4,attribut))
        calcul_pourcentage4(1000)
        