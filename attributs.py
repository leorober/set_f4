"""
Created on Thu Jan 13 15:04:17 2022

@author: Marie
"""
# Fichier comportant les fonctions pour trouver le CAP :
# pour 3 valeurs
# pour un nombre d'attributs variable

import numpy as np
import random as r
from itertools import product

#creation et initialisation d'un paquet de carte pour un nombre de :
#valeur val
#attribut att
def creation_paquet(val,att):
    jeu = []
    temp1 = []
    temp2 = []

    for i in range(val):
        temp1.append(i)
    for j in range(att):
        temp2.append(temp1)


    for elem in product(*temp2):
        jeu.append(elem)
        
    return jeu
    print(jeu)

#trouver la 3eme carte pour faire un SET
def trouver_set(carte1,carte2,val):
    
    carte_set=[]
    att = len(carte1)
    nb_eq = 0
    
    for i in range(att) :
        nb=(3-(carte1[i]+carte2[i]))%val
        nb_eq +=1
        carte_set.append(nb)
        
    return(carte_set, nb_eq)


#trouver la position d'une carte dans le paquet
def trouver_position(carte,val):
    
    position=0
    att = len(carte)
    
    for i in range(att):
        position += carte[i]*val**(att-1-i)
        
    return position



#Création de la liste des positions des cartes dans le jeu
# Liste des positions des cartes restantes à tirer
def creer_list_positions(val, att):
    
    n = val**(att)
    list_pos = []
    
    for i in range(n):
        list_pos.append(i)
        
    return(list_pos)


#dichotomie pour retrouver la position d'une carte.
def dicho(list_pos,position):
    ak = 0
    bk = len(list_pos)
    boolean = False
    
    while ak <= bk and boolean == False:
        
        milieu = (ak+bk)//2

        if list_pos[milieu] == position:
            boolean = True
        
        if position>list_pos[milieu]:
            ak = milieu + 1
        else :
            bk = milieu
            
    return milieu


#enlever une carte du jeu quand on a trouvé le set   
def enlever_carte(carte, jeu, list_pos, nb_cartes_supp,val):
    
    position=trouver_position(carte,val)
    
    if jeu[position]!=0 :
        jeu[position]=0
        supp = dicho(list_pos, position)
        del list_pos[supp]
        nb_cartes_supp+=1
    
    return (jeu,nb_cartes_supp)
 

#poser une carte sur un plateau
def poser_carte(jeu, list_pos:[int], plateau, val):

    nb_cartes_supp=0
    
    n=len(list_pos)
    i=r.randint(0,n-1)
    position=list_pos[i]
    del list_pos[i]
    carte1=jeu[position]
    
    nb_eq=0
    
    for j in range(len(plateau)):

        carte2=plateau[j]
        carte_set,temp =trouver_set(carte1,carte2,val)
        nb_eq +=temp
        jeu,nb_cartes_supp=enlever_carte(carte_set, jeu, list_pos, nb_cartes_supp,val)
    
    plateau.append(carte1)    
    jeu[position]=0   
    
    
    return (jeu, list_pos, plateau, nb_cartes_supp, nb_eq)


#trouver le cap pour une valeur donner 
def trouver_cap(val,att):
    
    jeu = creation_paquet(val,att)
    list_pos = creer_list_positions(val,att)
    plateau = []
    list_nb_cartes_supp = []
    nb_eq =0
    
    while len(list_pos) != 0:
        jeu,list_pos,plateau,nb_cartes_supp, temp = poser_carte(jeu, list_pos, plateau,val)
        nb_eq +=temp
        list_nb_cartes_supp.append(nb_cartes_supp)

    cap = len(plateau)
    return cap, nb_eq


#trouver le cap de taille maximale pour une valeur donnée
def trouver_cap_max(val,att):
    
    cap_max=0
    
    for k in range (5000):
        
        cap, nb_eq = trouver_cap(val, att)
        
        if cap>cap_max :
            cap_max = cap
            
    return (cap_max)


#Calculer les pourcentages des CAP pour 2 attributs et 3 valeurs
def calcul_pourcentage2(val,att,nbiteration):
    
    val=3
    att=2
    cap4=0
    
    for i in range(nbiteration):
        cap, nb_eq = trouver_cap(val,att)

        if cap==4 :
            cap4 +=1
    
    print ('cap de 4 :', cap4*100/nbiteration, '%')
    

#Calculer les pourcentages des CAP pour 3 attributs
def calcul_pourcentage3(val, att, nbiteration):
    
    cap8=0
    cap9=0
     
    for i in range(nbiteration):
        cap, nb_eq = trouver_cap(val,att)

        if cap==8 :
            cap8 +=1
            
        else:
            cap9 +=1
    
    print ('cap de 8 :', cap8*100/nbiteration, '%\ncap de 9 :', cap9*100/nbiteration,'%')
      

#Calculer les pourcentages des CAP pour 4 attributs
def calcul_pourcentage4(val, att, nbiteration): 
    
    cap15=0
    cap16=0
    cap17=0
    cap18=0
    cap19=0
    cap20=0 
     
    for i in range(nbiteration):
        cap, nb_eq = trouver_cap(val,att)
        
        if cap==15 :
            cap15 +=1
            
        elif cap==16 :
            cap16 +=1
            
        elif cap==17 :
            cap17 +=1
            
        elif cap==18 :
            cap18 +=1
            
        elif cap==19 :
            cap19 +=1
            
        elif cap==20 :
            cap20 +=1
        
    print ('cap de 15 :', cap15*100/nbiteration, '%\ncap de 16 :', cap16*100/nbiteration, '%\ncap de 17 :', cap17*100/nbiteration, '%\ncap de 18 :', cap18*100/nbiteration, '%\ncap de 19 :', cap19*100/nbiteration, '%\ncap de 20 :', cap20*100/nbiteration, '%')   
    
    
#Calculer les pourcentages des CAP pour 5 attributs    
def calcul_pourcentage5(val, att, nbiteration): 
    
    cap33=0    
    cap32=0    
    cap34=0
    cap35=0
    cap36=0
    cap37=0
    cap38=0
    cap39=0
    cap40=0
    cap41=0
    cap42=0
    cap43=0
    cap44=0
    cap45=0 
     
    for i in range(nbiteration):
        cap, nb_eq = trouver_cap(val,att)
        
        if cap==32 :
            cap32 +=1

        elif cap==33 :
            cap33 +=1
            
        elif cap==34 :
            cap34 +=1
            
        elif cap==35 :
            cap35 +=1
            
        elif cap==36 :
            cap36 +=1
            
        elif cap==37 :
            cap37 +=1
            
        elif cap==38 :
            cap38 += 1
            
        elif cap==39 :
            cap39 +=1
            
        elif cap==40 :
            cap40 +=1
            
        elif cap==41 :
            cap41 +=1
            
        elif cap==42 :
            cap42 +=1
            
        elif cap==43 :
            cap43 +=1
            
        elif cap==44 :
            cap44 +=1
            
        elif cap==45 :
            cap45 +=1

        
    print ('cap de 32 :', cap32*100/nbiteration, '%\ncap de 33 :', cap33*100/nbiteration, '%\ncap de 34 :', cap34*100/nbiteration, '%\ncap de 35 :', cap35*100/nbiteration, '%\ncap de 36 :', cap36*100/nbiteration, '%\ncap de 37 :', cap37*100/nbiteration, '%\ncap de 38 :', cap38*100/nbiteration, '%\ncap de 39 :', cap39*100/nbiteration, '%\ncap de 40 :', cap40*100/nbiteration, '%\ncap de 41 :', cap41*100/nbiteration, '%\ncap de 42 :',cap42*100/nbiteration, '%\ncap de 43 :', cap43*100/nbiteration, '%\ncap de 44 :',cap44*100/nbiteration, '%\ncap de 45 :', cap45*100/nbiteration, '%') 
    

#fonction principale du programme pour faire varier le nombre d'attributs avec 3 valeurs
def prog_principal():
    
    print('choisissez un nombre d attributs entre 2, 3, 4 et 5')
    temp = input()
    attribut = int(temp)
    
    if attribut==2 :
        print('le cap de taille maximale pour 3 valeurs et 2 attributs est', trouver_cap_max(3,attribut))
        calcul_pourcentage2(3,attribut,5000)
    
    elif attribut==3 :
        print('le cap de taille maximale pour 3 valeurs et 3 attributs est', trouver_cap_max(3,attribut))
        calcul_pourcentage3(3,attribut,5000)
    
    elif attribut==4 :
        print('le cap de taille maximale pour 3 valeurs et 4 attributs est', trouver_cap_max(3,attribut))
        calcul_pourcentage4(3,attribut,5000)
        
    else :
        print('le cap de taille maximale pour 3 valeurs et 5 attributs est', trouver_cap_max(3,attribut))
        calcul_pourcentage5(3,attribut,5000)