"""
Created on Thu Jan 13 15:04:17 2022

@author: Marie
"""

# Fichier contenant les fonctions permettant de trouver le cap avec des SET à 3 cartes
# pour 4 valeurs 
# pour 2, 3 ou 4 attributs

import numpy as np
import random as r
from itertools import product

#creation et initialisation d'un paquet de carte pour un nombre de :
#valeur val
#attribut att
def creation_paquet(val,att):
    jeu = []
    temp1 = []
    temp2 = []

    for i in range(val):
        temp1.append(i)
    for j in range(att):
        temp2.append(temp1)


    for elem in product(*temp2):
        jeu.append(elem)
        
    return jeu
    print(jeu)


#trouver les éléments manquants
def trouver_elts_manquants(a, b):
    
    l = list(range(4))
    res = []
    
    for i in range(len(l)):
        
        if (l[i]!=a and l[i]!=b) :
            res.append(l[i])
            
    return (res[0],res[1])


#produit cartésien 
def get_cart_prd(pools):
  result = [[]]
  for pool in pools:
    result = [x+[y] for x in result for y in pool]
  return result


#touver les 3eme cartes pour faire un SET (4 valeurs et 2/3 attributs)
def trouver_set3(carte1,carte2):
    
    cartes_set=[]
    att = len(carte2)
    cmp=0
    
    for i in range(att):
        if(carte1[i]!=carte2[i]):
            cmp += 1
    
    if(cmp==1):
        carte_set1=[]
        carte_set2=[] 
        
        for i in range(att) :
            
            if(carte1[i]==carte2[i]):
                nb=carte1[i]
                carte_set1.append(nb)
                carte_set2.append(nb)  
                
            else:
                nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
                carte_set1.append(nb1)
                carte_set2.append(nb2)
                
        cartes_set.extend([carte_set1, carte_set2])
                
    elif(cmp==2):
        carte_set1=[]
        carte_set2=[]
        carte_set3=[]
        carte_set4=[]
        cmp1=0
        
        for i in range(att) :  
                  
            if(carte1[i]==carte2[i]):
                nb=carte1[i]
                carte_set1.append(nb)
                carte_set2.append(nb)  
                carte_set3.append(nb)
                carte_set4.append(nb) 
                
            elif(carte1[i]!=carte2[i] and cmp1==0):
                nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
                carte_set1.append(nb1)
                carte_set2.append(nb1)
                carte_set3.append(nb2)
                carte_set4.append(nb2)
                cmp1+=1
                
            else:
                nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
                carte_set1.append(nb1)
                carte_set2.append(nb2)
                carte_set3.append(nb1)
                carte_set4.append(nb2) 
        cartes_set.extend([carte_set1, carte_set2, carte_set3, carte_set4])
                
    else:
        l=[]
        cartes_set=[]
        
        for i in range(att):
            
            temp = []
            nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
            temp.append(nb1)
            temp.append(nb2)
            l.append(temp)
            
        cartes_set=get_cart_prd(l)
            
    return cartes_set   


#touver les 3eme cartes pour faire un SET (4 valeurs et 4 attributs)
def trouver_set4(carte1,carte2):
    
    cartes_set=[]
    att = len(carte2)
    cmp=0
    
    for i in range(att):
        if(carte1[i]!=carte2[i]):
            cmp += 1
    
    if(cmp==1):
        carte_set1=[]
        carte_set2=[] 
        
        for i in range(att) :
            
            if(carte1[i]==carte2[i]):
                nb=carte1[i]
                carte_set1.append(nb)
                carte_set2.append(nb)  
                
            else:
                nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
                carte_set1.append(nb1)
                carte_set2.append(nb2)
                
        cartes_set.extend([carte_set1, carte_set2])
                
    elif(cmp==2):
        carte_set1=[]
        carte_set2=[]
        carte_set3=[]
        carte_set4=[]
        cmp1=0
        
        for i in range(att) :  
                  
            if(carte1[i]==carte2[i]):
                nb=carte1[i]
                carte_set1.append(nb)
                carte_set2.append(nb)  
                carte_set3.append(nb)
                carte_set4.append(nb) 
                
            elif(carte1[i]!=carte2[i] and cmp1==0):
                nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
                carte_set1.append(nb1)
                carte_set2.append(nb1)
                carte_set3.append(nb2)
                carte_set4.append(nb2)
                cmp1+=1
                
            else:
                nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
                carte_set1.append(nb1)
                carte_set2.append(nb2)
                carte_set3.append(nb1)
                carte_set4.append(nb2) 
        cartes_set.extend([carte_set1, carte_set2, carte_set3, carte_set4])
                
    elif (cmp==3):
        l=[]
        cartes_set=[]
        
        for i in range(att):
            
            if (carte1[i]!=carte2[i]):
                temp = []
                nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
                temp.append(nb1)
                temp.append(nb2)
                l.append(temp)
            else :
                position = i
            
        cartes_set=get_cart_prd(l)
        
        for j in range (len(cartes_set)):
            cartes_set[j].insert(position, carte1[position])
            
    else :
        l=[]
        cartes_set=[]
        
        for i in range(att):
            
            temp = []
            nb1, nb2 = trouver_elts_manquants(carte1[i], carte2[i])
            temp.append(nb1)
            temp.append(nb2)
            l.append(temp)

        cartes_set=get_cart_prd(l)
            
    return cartes_set    


#trouver la position d'une carte dans le paquet
def trouver_position(carte,val):
    
    position=0
    att = len(carte)
    
    for i in range(att):
        position += carte[i]*val**(att-1-i)
        
    return position


#Création de la liste des positions des cartes dans le jeu
# Liste des positions des cartes restantes à tirer
def creer_list_positions(val, att):
    
    n = val**(att)
    list_pos = []
    
    for i in range(n):
        list_pos.append(i)
        
    return(list_pos)


#dichotomie pour retrouver la position d'une carte.
def dicho(list_pos,position):
    ak = 0
    bk = len(list_pos)
    boolean = False
    
    while ak <= bk and boolean == False:
        
        milieu = (ak+bk)//2

        if list_pos[milieu] == position:
            boolean = True
        
        if position>list_pos[milieu]:
            ak = milieu + 1
        else :
            bk = milieu
            
    return milieu


#enlever une carte du jeu quand on a trouvé le set   
def enlever_carte(carte, jeu, list_pos, nb_cartes_supp,val):
    
    position=trouver_position(carte,val)
    
    if jeu[position]!=0 :
        jeu[position]=0
        supp = dicho(list_pos, position)
        del list_pos[supp]
        nb_cartes_supp+=1
    
    return (jeu,nb_cartes_supp)
 

#poser une carte sur un plateau
def poser_carte(jeu, list_pos:[int], plateau, val,att):

    nb_cartes_supp=0
    cartes_set=[]
    
    n=len(list_pos)
    i=r.randint(0,n-1)
    position=list_pos[i]
    del list_pos[i]
    carte1=jeu[position]
    
    
    for j in range(len(plateau)):

        carte2=plateau[j]
        if att==3 :
            cartes_set = trouver_set3(carte1,carte2)
        else :
            cartes_set = trouver_set4(carte1,carte2)
        
        for i in range(len(cartes_set)):
            jeu,nb_cartes_supp=enlever_carte(cartes_set[i], jeu, list_pos, nb_cartes_supp,val)
            
    plateau.append(carte1)    
    jeu[position]=0   
    
    return (jeu, list_pos, plateau, nb_cartes_supp)


#trouver le cap pour une valeur donner 
def trouver_cap(val,att):
    
    jeu = creation_paquet(val,att)
    list_pos = creer_list_positions(val,att)
    plateau = []
    list_nb_cartes_supp = []

    
    while len(list_pos) != 0:
        jeu,list_pos,plateau,nb_cartes_supp = poser_carte(jeu, list_pos, plateau,val,att)
        list_nb_cartes_supp.append(nb_cartes_supp)

    cap = len(plateau)
    return cap


#trouver le cap de taille maximale pour une valeur donnée
def trouver_cap_max(val,att):
    
    cap_max=0
    
    for k in range (5000):
        
        cap = trouver_cap(val, att)
        
        if cap>cap_max :
            cap_max = cap
            
    return (cap_max)


#Calculer les pourcentages des CAP pour 2 attributs et 4 valeurs
def calcul_pourcentage2(nbiteration):
    
    val=4
    att=2
    cap4=0
     
    for i in range(nbiteration):
        cap = trouver_cap(val,att)

        if cap==4 :
            cap4 +=1
    
    print ('cap de 4 :', cap4*100/nbiteration, '%')
      

#Calculer les pourcentages des CAP pour 3 attributs et 4 valeurs
def calcul_pourcentage3(nbiteration): 
    
    val=4
    att=3
    cap8=0
    cap9=0
    cap10=0
    cap11=0
    cap12=0

    for i in range(nbiteration):
        cap = trouver_cap(val,att)
            
        if cap==8 :
            cap8 +=1
            
        elif cap==9 :
            cap9 +=1
            
        elif cap==10 :
            cap10 +=1
            
        elif cap==11 :
            cap11 +=1
            
        elif cap==12 :
            cap12 +=1
        
    print ('cap de 8 :', cap8*100/nbiteration, '%\ncap de 9 :', cap9*100/nbiteration, '%\ncap de 10 :', cap10*100/nbiteration, '%\ncap de 11 :', cap11*100/nbiteration, '%\ncap de 12 :', cap12*100/nbiteration, '%')   
    
    
#Calculer les pourcentages des CAP pour 4 attributs et 4 valeurs
def calcul_pourcentage4(nbiteration): 
    
    val=4
    att=4
    cap15=0
    cap16=0
    cap17=0
    cap18=0
    cap19=0
    cap20=0
    cap21=0
    cap22=0
    cap23=0
    cap24=0
     
    for i in range(nbiteration):
        cap = trouver_cap(val,att)
            
        if cap==15 :
            cap15 +=1
            
        elif cap==16 :
            cap16 +=1
            
        elif cap==17 :
            cap17 +=1
            
        elif cap==18 :
            cap18 +=1
            
        elif cap==19 :
            cap19 +=1
            
        elif cap==20 :
            cap20 +=1
            
        elif cap==21 :
            cap21 +=1
            
        elif cap==22 :
            cap22 +=1
            
        elif cap==23 :
            cap23 +=1
            
        elif cap==24 :
            cap24 +=1     
        
    print ('cap de 15 :', cap15*100/nbiteration, '%\ncap de 16 :',  cap16*100/nbiteration, '%\ncap de 17 :', cap17*100/nbiteration, '%\ncap de 18 :', cap18*100/nbiteration, '%\ncap de 19 :', cap19*100/nbiteration, '%\ncap de 20 :', cap20*100/nbiteration, '%\ncap de 21 :', cap21*100/nbiteration, '%\ncap de 22 :', cap22*100/nbiteration, '%\ncap de 23 :', cap23*100/nbiteration, '%\ncap de 24 :',cap24*100/nbiteration, '%')    


#fonction principale du programme pour faire varier le nombre d'attributs avec 4 valeurs
def prog_principal():
    
    print('choisissez un nombre d attributs entre 2, 3 et 4')
    temp = input()
    attribut = int(temp)
    
    if attribut==2 :
        print('le cap de taille maximale pour 4 valeurs et 2 attributs est', trouver_cap_max(4,attribut))
        calcul_pourcentage2(1000)
    
    elif attribut==3 :
        print('le cap de taille maximale pour 4 valeurs et 3 attributs est', trouver_cap_max(4,attribut))
        calcul_pourcentage3(1000)
    
    else :
        print('le cap de taille maximale pour 4 valeurs et 4 attributs est', trouver_cap_max(4,attribut))
        calcul_pourcentage4(1000)
        
    